module.exports = {
  devtool: 'cheap-module-eval-source-map',

  entry: './src/index.ts',
  output: {
    filename: './dist/buf-hist-jump.js',
  },
  resolve: {
    extensions: ['', '.ts'],
  },
  module: {
    loaders: [
      { test: /\.ts$/, loaders: [
        'babel?plugins[]=transform-es2015-classes&plugins[]=transform-es2015-for-of',
        'ts-loader'
      ] },
    ],
  },

  ts: {
    ignoreDiagnostics: [2304, 2503],
  },
};
