interface WeakTab extends moz.xpcIJSWeakReference {
  get(): fx.Tab;
}
interface WeakGroupItem extends moz.xpcIJSWeakReference {
  get(): TabGroups.GroupItem;
}
