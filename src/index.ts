import { setup } from './buf-hist-jump';
const { assert } = liberator;


let teardown = null;
waitSessionReady()
  .then(tab => teardown = setup(tab))
  .catch(reason => console.error('Got error while waiting SessionStore to be ready and setup() to be done.', reason));


export function onUnload() {
  if (teardown) {
    teardown();
  }
}


function waitSessionReady(): Promise<fx.Tab> {
  let initialized = false;

  return Promise.race([
    waitSessionInitialized().then(() => {
      initialized = true;
      return null;
    }),
    waitSessionRestored().then(tab => {
      assert(!initialized, 'SessionStore must notify the RESTORED topic before resolves');
      return tab;
    }),
  ]);
}

function waitSessionInitialized() {
  debugger;
  assert(typeof SessionStore !== 'undefined', 'SessionStore must be something');
  const p = SessionStore.promiseInitialized;
  assert(p && typeof p.then === 'function', 'promiseInitialized must be a [[Promise]]');
  return p.then(Promise.resolve(null));
}

function waitSessionRestored() {
  return new Promise(resolve => {
    const obs: moz.nsIObserverService = services.get('obs');

    const startupObserver = {
      RESTORED: 'sessionstore-windows-restored',
      //RESTORED: 'sessionstore-state-finalized',

      init() {
        obs.addObserver(this, this.RESTORED, false);
      },
      uninit() {
        obs.removeObserver(this, this.RESTORED);
      },

      observe(subject, topic, data) {
        switch (topic) {
          case this.RESTORED:
            resolve(restoredTab());
            break;
        }
      },
    };

    startupObserver.init();
  });

  function restoredTab() {
    const { selectedTab } = getBrowser();
    if (selectedTab && !selectedTab.hidden) {
      return selectedTab;
    }
    return null;
  }
}
