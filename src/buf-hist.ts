import { settings } from './settings';
import { TabRef, isTabGroupActivatedTab } from './ref';
import { tabBrowser, isTabAlive } from './firefox';
import { GroupItemRef } from './ref';


const { assert } = liberator;

const METHOD_POPPING = Symbol('BufHist.popping()');

export const MigrateGroupedBufHist = Symbol('MigrateGroupedBufHist');


export interface BufHist {
  touchTab(tab: fx.Tab);
  forgetTab(tab: fx.Tab);

  getNthOlder(n: number, base: fx.Tab): TabRef;
}

class HistItem {
  tab: fx.Tab;
  ref: TabRef;
  older: HistItem;
  newer: HistItem;

  constructor(public tab: fx.Tab) {
  }

  get ref(): TabRef {
    const ref = TabRef.of(this.tab);
    //Reflect.defineProperty(this, 'ref', { enumerable: true, configurable: true, value: ref });
    Reflect.set(this, 'ref', ref);
    return ref;
  }

  static of(tab: fx.Tab) {
    return new HistItem(tab);
  }
}

export function createSingleBufHist(isGroupBufHist = false): BufHist {
  const history = new WeakMap<fx.Tab, HistItem>();
  let oldestHistItem: HistItem = null;
  let newestHistItem: HistItem = null;

  const ignoreDeadTab = isGroupBufHist;
  let definitelyLowerBound = ignoreDeadTab ? Number.NEGATIVE_INFINITY : 0;

  const singleBufHist = {
    touchTab,
    forgetTab,

    getNthOlder,

    [METHOD_POPPING]: popping,

    // For debugging
    [Symbol.iterator]() {
      console.log('Creating a iterator of HistItems for debugging.');
      return refs();

      function* refs() {
        for (const item of iterOfHist(tabBrowser.selectedTab)) {
          yield item.ref;
        }
      }
    },
  };
  Object.freeze(singleBufHist);
  return singleBufHist;

  function touchTab(tab: fx.Tab) {
    const histItem = history.get(tab);

    if (histItem) {
      assert(histItem.tab === tab);
      popHistItem(histItem);
      setAsNewest(histItem);
    } else {
      const newHistItem = HistItem.of(tab);
      definitelyLowerBound++;
      history.set(tab, newHistItem);
      setAsNewest(newHistItem);

      assert(definitelyLowerBound < tabBrowser.tabs.length, 'HistItem are definitely leaking.');
    }
  }

  function forgetTab(tab: fx.Tab) {
    const histItem = history.get(tab);
    if (histItem) {
      popHistItem(histItem);
    }
  }

  function getNthOlder(n: number, base: fx.Tab): TabRef {
    if (!isGroupBufHist && isTabGroupActivatedTab(base)) {
      throw MigrateGroupedBufHist;
    }

    const nth = Math.abs(n);
    let i = 0;

    for (const item: HistItem of iterOfHist(base, /*towardOlder=*/n > 0)) {
      const { tab } = item;
      const isAliveTab = isTabAlive(tab);

      if (ignoreDeadTab && !isAliveTab) {
        popHistItem(item);
        // then ignore
      } else if (settings.ignorePinnedTab && tab.pinned) {
        // ignore
      } else {
        assert(isAliveTab, 'The stored tab is dead. This means HistItem leaking.');

        if (i === nth) {
          return item.ref;
        }
        i++;
      }
    }

    return null;
  }


  function setAsNewest(histItem: HistItem) {
    rewireHistItem(histItem, newestHistItem);
    histItem.newer = null;
    newestHistItem = histItem;
  }

  function popHistItem(histItem: HistItem) {
    rewireHistItem(histItem.newer, histItem.older);
    definitelyLowerBound--;
  }

  function rewireHistItem(newer: HistItem, older: HistItem) {
    if (older) {
      older.newer = newer;
    } else {
      oldestHistItem = newer;
    }
    if (newer) {
      newer.older = older;
    }
  }

  function* iterOfHist(base: fx.Tab, towardOlder = true): Iterator<HistItem> {
    let item: HistItem = history.get(base);

    while (item) {
      const nextItem = towardOlder ? item.older : item.newer;
      yield item;
      item = nextItem;
    }
  }

  /**
   * Return an iterator which yields and removes all remembered items.
   * The iterator must be consumed entirely.
   * The remembered history should not be mutated during the iteration.
   */
  function* popping() {
    if (!oldestHistItem) {
      return;
    }

    for (const item of iterOfHist(oldestHistItem.tab, false)) {
      const deleted = history.delete(item.tab);
      assert(!ignoreDeadTab || deleted, 'HistItem must be deleted here');
      if (deleted) {
        definitelyLowerBound--;
      }
      yield item;
    }

    oldestHistItem = null;
    newestHistItem = null;
  }
}


export function migrateToGroupBufHist(singleHist: BufHist): BufHist {
  const poping = singleHist[METHOD_POPPING];
  const groupRefs = new WeakMap<TabGroups.GroupItem, GroupItemRef>();
  const histories = new WeakMap<GroupItemRef, BufHist>();

  const groupBufHist = {
    touchTab(tab: fx.Tab) {
      singleHist.touchTab(tab);
    },
    forgetTab(tab: fx.Tab) {
      singleHist.forgetTab(tab);

      // NOTE: Let the garbage collector automatically forget the corresponding HistItem
      //const hist = histories.get(tab);
      //if (hist) {
      //  hist.forgetTab(tab);
      //}
    },

    getNthOlder(n: number, base: fx.Tab) {
      merge();
      const hist = getHistOf(base);
      if (hist) {
        return hist.getNthOlder(n, base);
      }
      return null;
    },
  };
  Object.freeze(groupBufHist);
  return groupBufHist;

  function merge() {
    for (const item of poping()) {
      const { tab } = item;
      setAsNewest(tab);
    }
  }

  function setAsNewest(tab: fx.Tab) {
    const hist = getHistOf(tab);
    hist.touchTab(tab);
  }

  function getHistOf(tab: fx.Tab) {
    const groupRef = getGroupItemRefOf(tab);
    const hist = histories.get(groupRef);
    if (hist) {
      return hist;
    }

    const tabGroupHist = createSingleBufHist(true);
    histories.set(groupRef, tabGroupHist);
    return tabGroupHist;
  }

  function getGroupItemRefOf(tab: fx.Tab): GroupItemRef {
    const groupItem = GroupItemRef.getBelongingGroupItem(tab);
    const ref = groupRefs.get(groupItem);
    if (ref) {
      return ref;
    }

    const newRef = GroupItemRef.of(groupItem);
    groupRefs.set(groupItem, newRef);
    return newRef;
  }
}
