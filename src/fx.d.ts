declare namespace fx {
  interface Tab {
    label: string;
    selected: boolean;
  }
}
