import { isTabAlive } from './firefox';


const Cu: moz.nsIXPCComponents_Utils = Components.utils;
const weakOf = Cu.getWeakReference;

const { assert } = liberator;


export class GroupItemRef {
  private wrappedWeakGroupItem: WeakGroupItem;

  constructor(groupItem: moz.nsIDOMXULButtonElement) {
    this.wrappedWeakGroupItem = weakOf(groupItem);
  }

  static of(groupItem) {
    return new GroupItemRef(groupItem);
  }

  static getBelongingGroupItem(tab: fx.Tab): TabGroups.GroupItem {
    return tab._tabViewTabItem.parent;
  }
}


export class TabRef {
  private wrappedWeakTab: WeakTab;

  constructor(tab: fx.Tab) {
    this.wrappedWeakTab = weakOf(tab);
  }

  get tab(): fx.Tab {
    const tab = this.wrappedWeakTab.get();
    if (isTabAlive(tab)) {
      return tab;
    }
    return null;
  }

  get activeGroupRef(): GroupItemRef {
    const { tab } = this;
    if (!tab) {
      return null;
    }

    const tabItem = tab._tabViewTabItem;
    if (tabItem) {
      assert(tabItem.parent, 'TabItem must have its parent');
      return GroupItemRef.of(tabItem);
    }
    return null;
  }

  static of(tab: fx.Tab): TabRef {
    return new this(tab);
  }
}


export function isTabGroupActivatedTab(tab: fx.Tab) {
  return !!tab._tabViewTabItem;
}
