export const tabBrowser = getBrowser();

export function isTabAlive(tab: fx.Tab): boolean {
  return !!(tab && !tab.closing && tab.linkedBrowser);
}
