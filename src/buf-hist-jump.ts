import { BufHist, createSingleBufHist, migrateToGroupBufHist, MigrateGroupedBufHist } from './buf-hist';
import { TabRef } from './ref';
import { tabBrowser } from './firefox';


const { assert } = liberator;


interface BufJumper {
  touchTab(tab: fx.Tab);
  forgetTab(tab: fx.Tab);

  ignoreNext(tab: fx.Tab);

  getNthOlder(n: number, base: fx.Tab);

  [Symbol.iterator](): Iterator<TabRef>;
}


function createBufJumper(): BufJumper {
  let onceMigrated = false;

  let hist = createSingleBufHist();
  let ignoreNextTab = null;

  const jumper: BufJumper = {
    touchTab(tab: fx.Tab) {
      if (tab !== ignoreNextTab) {
        hist.touchTab(tab);
      }
      ignoreNextTab = null;
    },
    forgetTab(tab: fx.Tab) {
      hist.forgetTab(tab);
    },
    ignoreNext(tab: fx.Tab) {
      ignoreNextTab = tab;
    },
    getNthOlder,

    // For debugging
    [Symbol.iterator]() {
      return hist[Symbol.iterator]();
    }
  };
  Object.freeze(jumper);
  return jumper;

  function getNthOlder(n, base) {
    try {
      return hist.getNthOlder(n, base);
    } catch (e) {
      if (e === MigrateGroupedBufHist) {
        assert(!onceMigrated, 'MigrateGroupedBufHist is allowed only once.');
        onceMigrated = true;
        hist = migrateToGroupBufHist(hist);
        return hist.getNthOlder(n, base);
      }
      throw e;
    }
  }
}


function setupSingle(initialTab?: fx.Tab) {
  const bufJumper = createBufJumper();
  if (initialTab) {
    bufJumper.touchTab(initialTab);
  }

  const { tabContainer } = tabBrowser;

  tabContainer.addEventListener('TabSelect', onTabSelect);
  tabContainer.addEventListener('TabClose', onTabClose);

  function onTabSelect(e) {
    const tab = e.target;
    bufJumper.touchTab(tab);
  }
  function onTabClose(e) {
    const tab = e.target;
    bufJumper.forgetTab(tab);
  }

  bindMappings(bufJumper);

  return function unbind() {
    tabContainer.removeEventListener('TabSelect', onTabSelect);
  };
}


function bindMappings(bufJumper: BufJumper) {
  mappings.addUserMap(
    [modes.NORMAL],
    ['<C-h>'],
    'Focus the tab nth previously focused',
    selectOlder
  );
  mappings.addUserMap(
    [modes.NORMAL],
    ['<C-l>'],
    'Focus the tab nth recently focused',
    selectNewer
  );

  function selectOlder() {
    selectNth(1);
  }
  function selectNewer() {
    selectNth(-1);
  }

  function selectNth(n) {
    const before = bufJumper.getNthOlder(n, tabBrowser.selectedTab);
    if (!before) {
      return;
    }

    const tab = before.tab;
    if (tab) {
      bufJumper.ignoreNext(tab);
      tabBrowser.selectedTab = tab;
    }
  }
}


export function setup(initialTab?: fx.Tab) {
  const teardownSingle = setupSingle(initialTab);

  return function teardown() {
    teardownSingle();
  };
}

